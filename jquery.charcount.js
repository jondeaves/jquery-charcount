(function ($) {

    $.fn.charCounter = function (options) {
        var context = $(this),
            quantityCount = context.val().length,
            instance_id = Math.floor((Math.random()*1000)+1),
            is_min = false,
            is_max = false,
            append_ele = context;

        var defaults = {
            minQuantity : 0,
            maxQuantity : 0,
            submitBtn : null,
            change : null,
            inputGroup : false,
            allowMax : false,
            words : false,
            msgEle : null
        };
        var options = $.extend({}, defaults, options);


        if(options.inputGroup) { append_ele = append_ele.parent('div'); }


        // Bind to keydown and keyup to calculate character/word count and disable any buttons if needed
        context.keyup(function(){ val_change(context, options); });
        context.keydown(function(){ val_change(context, options); });

        // Actual function to count characters/words
        // remove characters/words if need and disable buttons if needed
        var val_change = function(context, options) {

            var quantityPhrase = (options.words) ? 'words' : 'characters';

            /* Count total letters if words is false, or entire words if words is true */
            quantityCount = 0;
            if(options.words && context.val().length > 0)
                quantityCount = countWords(context.val());
            else
                quantityCount = context.val().length;


            if(options.minQuantity != 0 && quantityCount >= options.minQuantity) { is_min = true; } else { is_min = false; }
            if(options.maxQuantity != 0 && quantityCount <= options.maxQuantity) { is_max = true; } else { is_max = false; }


            var callbackOptions = options;
            callbackOptions['is_min'] = is_min;
            callbackOptions['is_max'] = is_max;

            if(options.change != null) {
                options.change(callbackOptions);
            }




            if(((options.minQuantity != 0 && !callbackOptions.is_min) || (options.maxQuantity != 0 && !callbackOptions.is_max)) && options.submitBtn != null)
            {
                options.submitBtn.attr('disabled', true);
            } // close if((options.minQuantity != 0 && !callbackOptions.is_min) || (options.maxQuantity != 0 && !callbackOptions.is_max) && options.submitBtn != null)
            else if(options.submitBtn != null)
            {
                options.submitBtn.attr('disabled', false);
            } // close else if(options.submitBtn != null)


            if(!options.allowMax)
            {
                if(options.maxQuantity != 0 && !callbackOptions.is_max)
                {
                    context.val(context.val().substring(0, callbackOptions.maxQuantity));
                    quantityCount = options.maxQuantity;
                } // close if(options.maxQuantity != 0 && !callbackOptions.is_max)

                if(options.maxQuantity != 0 && quantityCount == options.maxQuantity && options.submitBtn != null)
                {
                    options.submitBtn.attr('disabled', false);
                }
            } // close if(!options.allowMax)

            var html_output = '',
                title = '',
                styles = '',
                remainingChars = options.maxQuantity - quantityCount;

            /* Build dynamic title based on whether we need maximum and minimum */
            if(options.minQuantity != 0) { title += 'Requires a minimum of '+options.minQuantity+' '+quantityPhrase+'. '; }
            if(options.maxQuantity != 0) { title += 'Requires a maximum of '+options.maxQuantity+' '+quantityPhrase+'. '; }


            /* BUild dynamic html + styles depending on wether given string has a length below minimum or on maximum */
            if(options.minQuantity != 0 && options.maxQuantity != 0) {
                if( quantityCount < options.minQuantity || quantityCount >= options.maxQuantity) { styles = 'color:#B94A48;'; }
                html_output = quantityCount+'/'+options.maxQuantity+' '+quantityPhrase;
            } else if(options.minQuantity != 0 && options.maxQuantity == 0) {
                if(quantityCount < options.minQuantity) { styles = 'color:#B94A48;'; }
                html_output = quantityCount+' '+quantityPhrase;
            } else if(options.minQuantity == 0 && options.maxQuantity != 0) {
                if(quantityCount >= options.maxQuantity) { styles = 'color:#B94A48;'; }
                html_output = remainingChars+' '+quantityPhrase+' left';
            } else {
                html_output = quantityCount+' '+quantityPhrase;
            }


            /*
             * Check if new element has been added already and update value otherwise
             *
             * Add text to new element beneath original if msgEle is null,
             * otherwise replace text in msgEle with new element.
             */
            if($('#counter_'+instance_id).length > 0) {
                $('#counter_'+instance_id).html(html_output);
            }
            else
            {
                var new_ele = $('<span id="counter_'+instance_id+'">'+html_output+'</span>');
                if(options.msgEle == null)
                    new_ele .insertAfter(append_ele);
                else
                    options.msgEle.html(new_ele)
            }

            $('#counter_'+instance_id).attr('title', title);
            $('#counter_'+instance_id).attr('style', styles);

        }

        var countWords = function(s){
            s = s.replace(/(^\s*)|(\s*$)/gi,"");//exclude  start and end white-space
            s = s.replace(/[ ]{2,}/gi," ");//2 or more space to 1
            s = s.replace(/\n /,"\n"); // exclude newline with a start spacing
            return s.split(' ').length;
        }

        val_change(context, options);

    }

})(jQuery);
